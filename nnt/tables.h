#pragma once

#ifndef TABLES_H
#define TABLES_H

struct head_one {
	int bolck_num;
	int unk1;
	int unk2;
	int unk3;
	int unk4;
	int pos_data_table;
	int unk6;
	int item_counts;
} head_one;

struct head_two {
	int bolck_num;
	int unk_block;

	//One XDB
	int block_index_one;
	int count_item;
	
	//NDB
	int position_ndb_tables;
	int ndb_struct_count;
	
	int position_three_data_block;
	int count_item_t;
	
	int unk_four_block;
	int count_item_tt;
	
	int unk10;
	int unk11;
} head_two;

struct table_index_xdb {
	int unk1;
	int position;
} m_table_index_xdb;

struct unk_block
{
	int pos;
	int unk2;

} m_unk_block;

#endif