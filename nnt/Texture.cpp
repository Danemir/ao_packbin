#include "common.h"
#include "Parsers.h"

int parse_texture(FILE* sfile, int position, std::string file_name) {
	
	fseek(sfile, position, SEEK_SET);

	Parser* Texture = new Parser(sfile, file_name, "Texture", 108);
	Texture->Skip(24);
	Texture->GetBool("wrap");
	Texture->GetInt("width");
	Texture->GetTextureType();
	Texture->GetInt("sourceFileCRC");
	Texture->GetInt("realWidth");
	Texture->GetInt("realHeight");
	Texture->GetInt("mipsNumber");
	Texture->GetInt("mipSW");
	Texture->GetInt("height");
	Texture->GetBoolMini("generateMipChain");
	Texture->GetBoolMini("disableLODControl",2);
	Texture->GetFileName("binaryFile2");
	Texture->GetFileName("binaryFile");
	Texture->GetBool("alphaTex");
	Texture->finish("Texture");

	return Texture->save();
}