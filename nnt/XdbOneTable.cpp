#include "XdbOneTable.h"

void XdbOneTable::init(int position) 
{
	setPosiion(position);
	m_tables_link = new tables_link();
	printf("### STAGE 2 ###\n");
	printf("Parsing XDB ONE TABlE\n");

	printf("Block num %i\n", getInt());
	Skip(4); //unk

	m_tables_link[0].id = 1;
	m_tables_link[0].offset = getPosiion();
	m_tables_link[0].position = getInt();
	printf("position table xdb list : %i\n", m_tables_link[0].position);
	printf("Full position xdb list %i\n", m_tables_link[0].position + m_tables_link[0].offset);
	count = getInt();
	printf("Count position xdb list %i\n", count);
	printf("######\n");

	m_tables_link[1].id = 2;
	m_tables_link[1].offset = getPosiion();
	m_tables_link[1].position = getInt();
	printf("position table NDB list : %i\n", m_tables_link[1].position);
	printf("Full position NDB list %i\n", m_tables_link[1].position + m_tables_link[1].offset);
	printf("Count position NDB list %i\n", getInt());
	printf("######\n");

	m_tables_link[2].id = 3;
	m_tables_link[2].offset = getPosiion();
	m_tables_link[2].position = getInt();
	printf("position table xdb two list : %i\n", m_tables_link[2].position);
	printf("Full position xdb two list %i\n", m_tables_link[2].position + m_tables_link[2].offset);
	printf("Count position xdb two list %i\n", getInt());
	printf("######\n");

	m_tables_link[3].id = 4;
	m_tables_link[3].offset = getPosiion();
	m_tables_link[3].position = getInt();
	printf("position table xdb three list : %i\n", m_tables_link[3].position);
	printf("Full position xdb three list %i\n", m_tables_link[3].position + m_tables_link[3].offset);
	printf("Count position xdb three list %i\n", getInt());
	printf("######\n");

	int b = getInt();
	m_tables_link[4].id = 5;
	m_tables_link[4].offset = getPosiion();
	m_tables_link[4].position = getInt();
	printf("position table xdb data : %i\n", m_tables_link[4].position);
	printf("Full position xdb data %i\n", m_tables_link[4].position + m_tables_link[4].offset);

	this->data_block = this->getPositionData();
	printf("Data position: %i \n", data_block);

	this->ParseOneTable();
	
	_getch();
}

int XdbOneTable::getPositionData() {
	setPosiion(m_tables_link[3].position + m_tables_link[3].offset);
	setPosiion((this->count - 1) * 8 + getPosiion());
	int offset = getPosiion();
	int position = getInt();
	int len = getInt();
	int dpos = (8 * len + 8) + offset + position;
	return dpos;
}

void XdbOneTable::ParseOneTable() {
	setPosiion(m_tables_link[0].position + m_tables_link[0].offset);
	int finish_table = (m_tables_link[0].position + m_tables_link[0].offset) + this->count * 8;

	for (int cx = 0;cx < count;cx++)
	{
		int offset = getPosiion();
		int position = getInt();
		int count = getInt();
		printf("File %i of %i\n", cx, this->count);
			this->ParseXdbBlock(offset + position, count, getPosiion());
	}
	ParseXdbData();
}

void XdbOneTable::ParseXdbBlock(int sposition, int count, int eposition) {
	setPosiion(sposition);

	for (int cx = 0; cx < count; cx++)
	{	
		int offset_name = getPosiion();
		int position_name = getInt();

		if (1 == position_name)
			position_name = 0;
		int len_name = getInt(); //hmm -10 wtf nival HACk?))

		int position = getInt();
		//GetName
		int fposition = getPosiion();
		setPosiion(offset_name + position_name);



		int unk1_1 = getInt();
		int unk1_2 = getInt();

		std::string fname = GetStr(len_name);
		printf("name: %s \n", fname.c_str());

		files[position + data_block].name = fname;
		this->ParseXdbData();
		setPosiion(fposition);

	}
	
	setPosiion(eposition);
}

void XdbOneTable::ParseXdbData()
{
	

	for (auto it = files.begin(); it != files.end(); ++it)
	{
		if (strstr(it->second.name.c_str(), "(Geometry)."))
			parse_Geometry(this->sfile, it->first, it->second.name);

		//if (strstr(it->second.name.c_str(), "(Texture)."))
		//	parse_texture(this->sfile, it->first, it->second.name);

		//if (strstr(name.c_str(), "(UITexture)."))
		//	parse_UITexture(this->sfile, position, name);

	//	if (strstr(name.c_str(), "(SkeletalAnimation)."))
		//	parse_SkeletalAnimation(this->sfile, position, name);
	}
}

XdbOneTable::~XdbOneTable()
{

}