#include "common.h"
#include "Parsers.h"

void parse_UITexture(FILE* sfile, int position, std::string file_name) {	
	fseek(sfile, position, SEEK_SET);
	Parser* UITexture = new Parser(sfile, file_name, "UITexture", 108);
	UITexture->Skip(24);
	UITexture->GetBool("wrap");
	UITexture->GetInt("width");
	UITexture->GetTextureType();
	UITexture->GetInt("sourceFileCRC");
	UITexture->GetInt("realWidth");
	UITexture->GetInt("realHeight");
	UITexture->GetInt("mipsNumber");
	UITexture->GetInt("mipSW");
	UITexture->GetInt("height");
	UITexture->GetBoolMini("generateMipChain");
	UITexture->GetBoolMini("disableLODControl", 2);
	UITexture->GetFileName("binaryFile2");
	UITexture->GetFileName("binaryFile");
	UITexture->GetBool("alphaTex");
	UITexture->finish("UITexture");
	UITexture->save();
	delete(UITexture);
}