#include "OneTable.h"

void OneTable::init() 
{
	m_tables_link = new tables_link();
	printf("### STAGE 1 ###\n");
	printf("Parsing ONE TABlE\n");
	printf("Block_num: %i\n", getInt());

	this->Skip(16); //Skip not rec data
	
	m_tables_link[0].id = 1;
	m_tables_link[0].position = getInt();
	m_tables_link[0].offset = getPosiion();

	printf("position table xdb list one: %i\n", m_tables_link[0].position);
	printf("Full position xdb list one %i\n", m_tables_link[0].position + m_tables_link[0].offset);

	m_tables_link[1].id = 2;
	m_tables_link[1].offset = getPosiion();
	m_tables_link[1].position = getInt();
	

	printf("position for loc list table %i\n", m_tables_link[1].position);
	printf("Full position loc list table %i\n", m_tables_link[1].position + m_tables_link[1].offset);
	this->count = getInt();
	printf("count locales file's: %i\n", this->count);
	printf("Finish locales files position %i\n", m_tables_link[1].position + m_tables_link[1].offset + (this->count * 12));
	endTable = m_tables_link[1].position + m_tables_link[1].offset + (this->count * 12);
	printf("### STAGE parse ###\n");
	//this->ParseTable();
	printf("### STAGE 1  FINISH ###\n");

	sXdbOneTable->init(m_tables_link[0].position + m_tables_link[0].offset);
}

void OneTable::ParseTable()
{
	setPosiion(m_tables_link[1].position + m_tables_link[1].offset);
	for (int cx = 0;cx < count;cx++)
	{
		int offset = getPosiion();
		int position = getInt();
		int len = getInt(); //len = 1 is dir skip	
		Skip(4);

		if(len>1)
		ParseData(offset + position, len, getPosiion());
	}
}

void OneTable::ParseData(int sposition, int len, int eposition)
{
	setPosiion(sposition);
	std::string file = GetStr(len);
	#ifdef _DEBUG
		printf("%s\n", file.c_str());
	#endif
		
		boost::filesystem::path p{ create_dir_on_patch(file).c_str() };
		boost::filesystem::ofstream(p, std::ios_base::app);

	setPosiion(eposition);
}

OneTable::~OneTable()
{

}