#include "Parsers.h"

int Parse_SceneNodes(FILE* file, int position, char* type, int body_len) {

	Parser* SceneNodes = new Parser(file, position, "SceneNodes", body_len-4);
	int sceneNodesCount = body_len / 48;

	for (int cx = 0; cx < sceneNodesCount;cx++)
	{
		SceneNodes->start("Item");
		SceneNodes->GetFloat("scale");
		SceneNodes->GetQuat("rotation");
		SceneNodes->GetVEC("position"); //NEED TODOO
		SceneNodes->GetStr("name");
		SceneNodes->Skip(4);
		SceneNodes->finish("Item");
	}

	return SceneNodes->getDataPosiion();
}