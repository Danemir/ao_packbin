#pragma once

//6
struct SkeletalAnimation {

};



struct Texture_xdb {
	int unk;
	int pos;
	int unk2;
	int unk3;
};

struct Texture_data {

	int unk1;// -00 00 00 00
	int unk2;//	- 01 00 00 00
	int unk3;//	- 00 00 00 00
	int unk4;//	- 00 00 00 00
	int unk5;//	- 01 00 00 00
	int unk6;//	- 00 00 00 00
	int unk7; //	- 00 00 00 00
	int width;//	- 00 08 00 00 width !
	int type;//	- 02 00 00 00 type(dxt5:2, dxt11 : 0) !
	int unk8;//	- 00 00 00 00
	int realWidth;//	- 00 08 00 00 realWidth
	int realHeight;//	- 00 08 00 00 realHeight
	int mipsNumber;//	- 0a 00 00 00 mipsNumber !
	int mipSW;//	- ff ff ff ff mipSW !
	int height;//	- 00 08 00 00 height
	int generateMipChain;//	- 01 00 00 00 generateMipChain(true / false)
	int unk9;//	- 00 08 00 00
	int unk10;//	- 2c 00 00 00
	int unk11;//	- 39 00 00 00
	int unk12;//	- 17 00 00 00
	int unk13;//	- 00 08 00 00
	int unk14;//	- 00 08 00 00
	int unk15;//	- 29 00 00 00
	int unk16;//	- 39 00 00 00
	int unk17;//	- 18 00 00 00
	int unk18;//	- 00 00 00 00
	int alphaTex;//	- 01 00 00 00 alphaTex(true / false)
} m_Texture_data;